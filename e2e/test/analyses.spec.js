import Issue from './pages/Issue'

describe('Analyses', () => {
  it('create and remove Positives', () => {
    const issue = new Issue()
      .createIssue()
      .openPositiveForm()
      .fillPositiveWith("It's positive")
      .confirmPositiveWithEnter()
      .fillPositiveWith("Another positive")
      .confirmPositiveWithEnter()
      .fillPositiveWith("Positive")
      .confirmPositiveWithEnter()
      .cancelPositive()
      .deleteFirstPositive()

    expect(issue.hasNumberOfPositives(2)).to.be.true
  })

  it('create and remove Negative', () => {
    const issue = new Issue()
      .createIssue()
      .openNegativeForm()
      .fillNegativeWith("It's negative")
      .confirmNegativeWithEnter()
      .fillNegativeWith("Another negative")
      .confirmNegativeWithEnter()
      .fillNegativeWith("Negative")
      .confirmNegativeWithEnter()
      .cancelNegative()
      .deleteFirstNegative()

    expect(issue.hasNumberOfNegatives(2)).to.be.true
  })
})
