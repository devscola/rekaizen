const ADD_LESSON_LEARNED = '#add-lessons-learned'
const ADD_NEW_GOOD_PRACTICE_BUTTON = '#new-good-practice'
const ADD_NEW_LESSON_LEARNED = '#new-lessons-learned'
const POSITIVE_ADD = '#positives-add'
const POSITIVE_INPUT = '#positives textarea'
const FIRST_POSITIVE = '#positives-0'
const POSITIVES_LIST = '#positives-list'
const NEGATIVE_INPUT = '#negatives textarea'
const NEGATIVES_LIST = '#negatives-list'
const FIRST_NEGATIVE = '#negatives-0'
const NEGATIVE_ADD = '#negatives-add'
const ISSUE_PATH = '/'
const TIMEBOX_OPTION_15 = '#timebox-option-15'
const IMPROVEMENT_ACTION_SHOW = '#improvement-actions-show'
const IMPROVEMENT_ACTION_ADD = '#improvement-actions-add'
const IMPROVEMENT_ACTION_CANCEL = '#improvement-actions-cancel'
const FIRST_IMPROVEMENT_ACTION = '#improvement-actions-0'
const WELCOME_BUTTON = '#confirm-landing'
const DOWNLOAD_SUMMARY = '#button-summary'
const FINISH = '#create-report-button'
const DESCRIPTION_ISSUE_INPUT = '#description-issue-input'
const CONFIRM_ISSUE_BUTTON ='#confirm-issue'

class Issue {
  constructor() {
    cy.visit(ISSUE_PATH)

    cy.get(ADD_NEW_GOOD_PRACTICE_BUTTON)
    cy.get(ADD_NEW_LESSON_LEARNED)
    cy.get(POSITIVE_ADD)
    cy.get(NEGATIVE_ADD)
    cy.get(IMPROVEMENT_ACTION_SHOW)
    cy.get(FINISH)
  }

  startIssue(){
    cy.get(WELCOME_BUTTON).click()
    return this
  }

  addDescription(text = 'description') {
    cy.get(DESCRIPTION_ISSUE_INPUT).type(text)
    cy.get(DESCRIPTION_ISSUE_INPUT).type('{enter}')

    return this
  }

  selectTimebox() {
    cy.get(TIMEBOX_OPTION_15).click()
  }

  confirmIssue() {
    cy.get(CONFIRM_ISSUE_BUTTON).click()

    return this
  }

  createIssue() {
    this.startIssue()
    this.addDescription()
    this.selectTimebox()
    this.confirmIssue()

    return this
  }

  openNewGoodPractice() {
    cy.get(ADD_NEW_GOOD_PRACTICE_BUTTON).click()

    return this
  }

  openNewLessonLearned() {
    cy.get(ADD_NEW_LESSON_LEARNED).click()

    return this
  }

  hasCSSAtribute(selector, cssAttribute) {
    cy.get(selector).should('have.css', cssAttribute)
    return true
  }

  closeConclusionModal() {
    cy.get('.modal-column .close').click()

    return this
  }

  fillConclusion(text) {
    cy.get('.modal-column textarea:first').type(text)

    return this
  }

  addConclusion() {
    cy.get('.modal-column button[type=submit]').click()

    return this
  }

  openPositiveForm() {
    cy.get(POSITIVE_ADD).click()

    return this
  }

  fillPositiveWith(description) {
    cy.get(POSITIVE_INPUT).type(description)

    return this
  }

  confirmPositiveWithEnter() {
    cy.get(POSITIVE_INPUT).type('{enter}')

    return this
  }

  cancelPositive() {
    cy.get(POSITIVE_INPUT).type('{esc}')

    return this
  }

  deleteFirstPositive() {
    cy.get(FIRST_POSITIVE + ' button').click({ force: true })

    return this
  }


  hasNumberOfPositives(number) {
    cy.get(POSITIVES_LIST).find('.list-item').should('have.length', number)

    return true
  }

  openNegativeForm() {
    cy.get(NEGATIVE_ADD).click()

    return this
  }

  fillNegativeWith(description) {
    cy.get(NEGATIVE_INPUT).type(description)

    return this
  }

  confirmNegativeWithEnter() {
    cy.get(NEGATIVE_INPUT).type('{enter}')

    return this
  }

  cancelNegative() {
    cy.get(NEGATIVE_INPUT).type('{esc}')

    return this
  }

  deleteFirstNegative() {
    cy.get(FIRST_NEGATIVE + ' button').click({ force: true })

    return this
  }

  hasNumberOfNegatives(number) {
    cy.get(NEGATIVES_LIST).find('.list-item').should('have.length', number)

    return true
  }

  includes(text) {
    cy.contains(text)

    return true
  }

  notInclude(text) {
    cy.get(text).should('not.exist')

    return true
  }

  isVisible(selector) {
    cy.get(selector).should('be.visible')
    return true
  }

  isNotOpenedAddLessonLearned() {
    cy.get('#modal-add-lessons-learned').should('not.exist')

    return true
  }

  cannotAddLessonLearned() {
    cy.get(ADD_LESSON_LEARNED).should('have.attr', 'disabled')

    return true
  }

  clickOnMoreOptions() {
    cy.get('#options').click()

    return this
  }

  clickCreateReport(){
    cy.get('#create-report-button').click()

    return this
  }

  containDownloadSummary(){
    cy.get(DOWNLOAD_SUMMARY)

    return true
  }

  showImprovementActionForm() {
    cy.get(IMPROVEMENT_ACTION_SHOW).click()

    return this
  }

  fillImprovementAction(description) {
    cy.get('#improvement-actions form textarea').type(description)
    return this
  }

  addImprovementAction() {
    cy.get(IMPROVEMENT_ACTION_ADD).click()

    return this
  }

  cancelImprovementAction() {
    cy.get(IMPROVEMENT_ACTION_CANCEL).click()

    return this
  }

  deleteFirstImprovementAction() {
    cy.get( FIRST_IMPROVEMENT_ACTION + ' button').click({ force: true })

    return this
  }

  hasFirstLessonsLearned() {
    cy.get('#lessons-learned-0')

    return true
  }

  hasFirstPositive() {
    cy.get('#positives-0')

    return true
  }
}

export default Issue
