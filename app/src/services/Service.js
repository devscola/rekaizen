
class Service {
  constructor(bus) {
    this.bus = bus

    this.subscriptions()
  }
  subscriptions() {}

  buildCallback(signal) {
    return response => {
      this.bus.publish(signal, response)
    }
  }
}

export default Service
