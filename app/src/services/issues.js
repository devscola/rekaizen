import Actions from '../api/actions'
import Service from './Service'

export default class Issues extends Service {
  constructor(bus) {
    super(bus)
  }

  subscriptions() {
    this.bus.subscribe('create.issue', this.createIssue.bind(this))
  }

  createIssue(payload) {
    let callback = this.buildCallback('created.issue')

    const issue = Actions.createIssue.do(payload)

    callback(issue)
  }
}
