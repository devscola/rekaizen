import Actions from '../api/actions'
import Service from './Service'
import getLanguage from '../libraries/getLanguage'

class Reports extends Service {
  subscriptions() {
    this.bus.subscribe('generate.report', this.generateReport.bind(this))
  }

  generateReport(payload) {
    const language = getLanguage()
    const callback = this.buildCallback('generated.report')

    const report = Actions.generateReport.do(payload.issue, language)

    callback(report)
  }
}

export default Reports
