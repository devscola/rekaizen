import Actions from '../api/actions'
import Service from './Service'

export default class ImprovementActions extends Service {
  subscriptions() {
    this.bus.subscribe('retrieve.improvementActions', this.retrieveAllFor.bind(this))
    this.bus.subscribe('create.improvementAction', this.create.bind(this))
    this.bus.subscribe('remove.improvementAction', this.remove.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.improvementAction')

    const improvementAction = Actions.createImprovementAction.do(payload)

    callback(improvementAction)
  }

  retrieveAllFor({ issue }){
    let callback = this.buildCallback('retrieved.improvementActions')

    const improvementActions = Actions.retrieveImprovementActions.do(issue)

    callback(improvementActions)
  }

  remove(payload) {
    let callback = this.buildCallback('removed.improvementAction')

    const issue = Actions.removeImprovementAction.do(payload)

    callback(issue)
  }
}
