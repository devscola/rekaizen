import React from 'react'
import LogoRekaizen from './LogoRekaizen'
import LogoRekaizenLoaded from './LogoRekaizenLoaded'

class Timebox extends React.Component{
  render() {
    const minuteInSeconds = 60
    let animation = {}
    if (this.props.timebox) {
      animation = {
        animationDuration: `${this.props.timebox * minuteInSeconds}s`,
        animationFillMode: this.props.timebox ? 'forwards' : 'backwards',
        animationPlayState: this.props.timebox ? 'running' : 'initial',
        animationName: 'load',
        animationIterationCount: 1,
        animationTimingFunction: 'linear'
      }
    }
    return (
      <div className="logo-wrapper">
        <div className="logo no-print" style={animation}>
          <LogoRekaizen timebox={this.props.timebox} color="white" />
        </div>
        <div className="logo-loader">
          <LogoRekaizenLoaded timebox={this.props.timebox} />
        </div>
      </div>
    )
  }
}

export default Timebox;
