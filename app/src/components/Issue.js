import React from 'react'
import Modal from './Modal'
import IssueDescription from './IssueDescription'
import IssueTimebox from './IssueTimebox'
import IssueConfirm from './IssueConfirm'

const STATE_DESCRIPTION = 0
const INITIAL_STATE = {
  description: null,
  timebox: null,
  step: 0,
  translateValue: 0
}

class Issue extends Modal {
  constructor(props) {
    super(props)
    this.totalSteps = 3
    this.state = INITIAL_STATE
  }

  extraStyle() {
    return 'create-issue'
  }

  createIssue() {
    this.props.createIssue({
      description: this.state.description,
      timebox: this.state.timebox,
    })
    this.setState(INITIAL_STATE)
  }

  addDescription(description) {
    this.setState({ description })
    if(this.state.step === STATE_DESCRIPTION) {
      this.nextStep()
    }
  }

  addTimebox(timebox) {
    this.setState({ timebox })
    this.nextStep()
  }

  previousStep() {
    this.setState(prevState => ({
      step: prevState.step - 1,
      translateValue: prevState.translateValue + this.slideWidth()
    }))
  }

  nextStep() {
    this.setState(prevState => ({
      step: prevState.step + 1,
      translateValue: prevState.translateValue + -(this.slideWidth())
    }))
  }

  slideWidth() {
    return document.querySelector('.slide').clientWidth
  }

  content() {
    return (
      <div
        className="slider"
        style={{
          transform: `translateX(${this.state.translateValue}px)`,
          transition: 'transform ease-out 0.45s',
          width: `${100*this.totalSteps}%`
        }}
      >
        <IssueDescription
          translations={this.props.translations}
          onSubmit={this.addDescription.bind(this)}
        />
        <IssueTimebox
          translations={this.props.translations}
          options={this.props.timeboxOptions}
          moreOptions={this.props.timeboxMoreOptions}
          onSubmit={this.addTimebox.bind(this)}
          back={this.previousStep.bind(this)}
        />
        <IssueConfirm
          timebox={this.state.timebox}
          description={this.state.description}
          onSubmit={this.createIssue.bind(this)}
          back={this.previousStep.bind(this)}
          translations={this.props.translations}
        />
      </div>
    )
  }
}

export default Issue
