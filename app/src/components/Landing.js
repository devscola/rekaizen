import React from 'react'
import Modal from './Modal'
import LogoRe from './LogoRe'
import Button from './Button'

class Landing extends Modal {
  content() {
    return (
      <React.Fragment>
        <LogoRe />
        <div className="modal-container">
          <h2 className="title">
            {this.props.translations.landingSlogan}
          </h2>
          <p className="subtitle landing-description">
            {this.props.translations.landingCopy}
          </p>
          <p className="text text-ceremonial">{
            this.props.translations.landingExplanation}
          </p>

          <Button
            ceremonial
            id={"confirm-" + this.props.id}
            onClick={this.props.submit}
          >
            {this.props.translations.start}
          </Button>
        </div>
      </React.Fragment>
    )
  }
}

export default Landing
