import React from "react"
import LogoRe from './LogoRe'
class IssueDescription extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      description: ''
    }
  }

  handleDescription(event) {
    this.setState({ description: event.target.value})
  }

  hasDescription() {
    return this.state.description.length > 0
  }

  handleSubmit(event) {
    event.preventDefault()
    this.props.onSubmit(this.state.description)
  }

  render() {
    const disabledButton = !this.hasDescription()
    return (
      <div className="slide">
        <LogoRe />
        <div className="modal-container">
          <h2 className="title">{this.props.translations.issueTitle}</h2>
          <p className="text landing-description">
            {this.props.translations.issueSubtitle}
          </p>
          <form onSubmit={this.handleSubmit.bind(this)} className="description-issue" >
            <input
              type="text"
              id="description-issue-input"
              autoComplete="off"
              autoFocus
              value={this.props.description}
              onChange={this.handleDescription.bind(this)}
              className="description-issue-input"
            />
            <button
              id='description-issue-button'
              type="submit"
              disabled={disabledButton}
              className="description-issue-button"
            >
              <i className="fas fa-chevron-right fa-lg" />
            </button>
          </form>
        </div>
      </div>
    )
  }
}

export default IssueDescription
