import React from 'react'
import Button from './Button'

class ReportMarkdown extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showMessage: false,
    }
  }
  listItems(conclusions) {
    return conclusions.map((item, index) => `- ${item} `).join('\n')
  }
  reportText() {
    return [
      `# ${this.props.issueDescription}`,
      `${this.props.explanation.introduction.replace(/"/g, '**')}`,
      `${this.props.explanation.improvementActionsIntro}`,
      `${this.listItems(this.props.explanation.improvementActions)}`,
      `${this.props.explanation.goodPracticesIntro}`,
      `${this.listItems(this.props.explanation.goodPractices)}`,
      `${this.props.explanation.lessonsLearnedIntro}`,
      `${this.listItems(this.props.explanation.lessonsLearned)}`,
      `${this.props.explanation.ending}`,
    ]
      .filter(item => item != '')
      .join('\n\n')
  }
  copyToClipBoard() {
    navigator.clipboard.writeText(this.reportText())
    this.setState({showMessage: true})
  }
  renderMessage() {
    if (!this.state.showMessage) {
      return null
    }
    return (
      <p className="has-text-ceremonial">
        {this.props.translations.copyTextSuccessful}
      </p>
    )
  }
  render() {
    return (
      <React.Fragment>
        <p className="has-text-centered">
          <Button ceremonial onClick={this.copyToClipBoard.bind(this)}>
            {this.props.translations.copyMarkdown}
          </Button>
        </p>
        {this.renderMessage()}
      </React.Fragment>
    )
  }
}

export default ReportMarkdown
