import GoodPractices from './GoodPractices'
import ImprovementActions from './ImprovementActions'
import LessonsLearned from './LessonsLearned'
import Positives from './Positives'
import Negatives from './Negatives'
import Navbar from './Navbar'
import Report from './Report'
import Landing from './Landing'
import Issue from './Issue'

class Containers {
  static report(bus){
    return new Report(bus)
  }

  static improvementActions(bus) {
    return new ImprovementActions(bus)
  }

  static goodPractices(bus) {
    return new GoodPractices(bus)
  }

  static lessonsLearned(bus) {
    return new LessonsLearned(bus)
  }

  static positives(bus) {
    return new Positives(bus)
  }

  static negatives(bus) {
    return new Negatives(bus)
  }

  static navbar(bus) {
    return new Navbar(bus)
  }

  static landing(bus) {
    return new Landing(bus)
  }

  static issue(bus) {
    return new Issue(bus)
  }
}

export default Containers
