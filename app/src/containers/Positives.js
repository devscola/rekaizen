import Container from './Container'
import Renderers from '../renderers'
import Orderer from '../libraries/Orderer'

class Positives extends Container {
  constructor(bus){
    super(bus)
    const callbacks = {
      create: this.createPositive.bind(this),
      remove: this.removePositive.bind(this)
    }
    this.renderer = Renderers.positives(callbacks)
  }

  subscriptions(){
    this.bus.subscribe('created.issue', this.retrieveIssue.bind(this))
    this.bus.subscribe('restore.issue', this.askPositives.bind(this))
    this.bus.subscribe("got.translations", this.updateTranslations.bind(this))
    this.bus.subscribe("retrieved.positives", this.updatePositives.bind(this) )
    this.bus.subscribe("removed.positive", this.askCollections.bind(this))
    this.bus.subscribe("created.positive", this.askCollections.bind(this))
    this.bus.subscribe('new.issue', this.clean.bind(this))
  }

  askPositives(issue) {
    this.retrieveIssue(issue)
    this.bus.publish("retrieve.positives", {issue: this.issue.id})
  }

  askCollections() {
    this.bus.publish("retrieve.positives", {issue: this.issue.id})
  }

  updatePositives(payload) {
    let orderedPayload = Orderer.byBirthdayOldestToNewest(payload)
    this.renderer.update(orderedPayload)
  }

  createPositive(value) {
    let payload = { description: value, issue: this.issue.id }
    this.renderer.updateOne(payload)
    this.bus.publish("create.positive", payload)
  }

  removePositive(positive) {
    const payload = positive
    payload['issue'] = this.issue.id

    this.bus.publish('remove.positive', payload)
  }

  clean() {
    this.retrieveIssue({})
    this.renderer.update([])
  }

  render() {
    this.renderer.draw()
  }
}

export default Positives
