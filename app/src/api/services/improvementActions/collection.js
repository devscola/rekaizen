import ImprovementAction from '../../domain/ImprovementAction'

let collection = []

export default class Collection {
  static retrieveAll() {
    return collection
  }

  static insert(improvementAction) {
    collection.push(improvementAction.serialize())

    return improvementAction
  }

  static retrieve(references) {
    let asked = []

    references.forEach((reference) => {
      collection.forEach((item) => {
        const improvementAction = ImprovementAction.from(item)
        if(improvementAction.isEqual({id: reference})) {

          asked.push(improvementAction)
        }
      })
    })

    return asked
  }

  static remove(id) {
    const totalCollection = collection.length
    collection = collection.filter((item) => item.id != id)

    return totalCollection != collection.length
  }

  static drop() {
    collection = []
  }
}
