export default class Collection {
  static retrieveAll () {
    return {
      options: [5, 10, 15, 20, 25, 30],
      moreOptions: [35, 40, 45, 50, 55, 60]
    }
  }
}
