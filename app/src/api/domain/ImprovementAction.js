import SecureRandom from '../libraries/SecureRandom'
import Dates from '../libraries/Dates'

export default class ImprovementAction {
  static from({description, id, birthday}) {
    const improvementAction = new ImprovementAction(description)

    improvementAction.id = id
    improvementAction.birthday = birthday

    return improvementAction
  }

  constructor(description) {
    this.description = description
    this.id = this.newUuid()
    this.birthday = this.generateTimestamp()
  }

  isEqual(other) {
    return (this.id == other.id)
  }

  serialize() {
    return {
      id: this.id,
      birthday: this.birthday,
      description: this.description
    }
  }

  newUuid() {
    return SecureRandom.uuid()
  }

  generateTimestamp() {
    return Dates.generateTimestamp()
  }
}
