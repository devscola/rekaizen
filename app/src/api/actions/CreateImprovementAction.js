import ImprovementActions from '../services/improvementActions'
import Issues from '../services/issues'

export default class CreateImprovementAction {
  static do(payload) {
    const improvementAction = ImprovementActions.service.create(payload.description)
    Issues.service.addImprovementAction(payload.issue, improvementAction.id)

    return improvementAction
  }
}
