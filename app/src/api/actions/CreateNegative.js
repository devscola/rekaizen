import Issues from '../services/issues'

export default class CreateNegative {
  constructor(bus) {
    this.bus = bus

    this.bus.subscribe('create.negative', this.do.bind(this))
  }

  do(negative) {
    const newNegative = Issues.service.createNegative(negative.issue, negative.description)

    this.bus.publish('created.negative', newNegative)
  }
}
