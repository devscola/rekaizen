import getLanguage from '../../libraries/getLanguage'
import Translations from '../services/translations'

export default class TranslateAll {
  constructor(bus) {
    this.bus = bus

    this.do()
  }

  do() {
    const translations = Translations.service.retrieveAll()

    this.bus.publish('got.translations', translations[getLanguage()])
  }
}
