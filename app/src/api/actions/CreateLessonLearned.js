import Conclusions from '../services/conclusions'
import Issues from '../services/issues'

export default class CreateLessonLearned {
  constructor(bus) {
    this.bus = bus

    this.bus.subscribe('create.lessonLearned', this.do.bind(this))
  }

  do(lessonLearned) {
    const newLessonLearned = Conclusions.service.createLessonLearned(lessonLearned.description)

    Issues.service.addConclusion(lessonLearned.issue, newLessonLearned.id)

    this.bus.publish('created.lessonLearned', newLessonLearned)
  }
}
