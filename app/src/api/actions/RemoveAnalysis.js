import Issues from '../services/issues'

export default class RemoveAnalysis {
  static do(analysis) {
    const issue = Issues.service.removeAnalysis(analysis.issue, analysis.id)

    return issue
  }

  constructor(bus) {
    this.bus = bus

    this.bus.subscribe('remove.negative', this.do.bind(this))
  }

  do(analysis) {
    const issue = RemoveAnalysis.do(analysis)

    this.bus.publish('removed.negative', issue)
  }
}
