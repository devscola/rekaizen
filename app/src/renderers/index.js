import ImprovementActions from './ImprovementActions'
import GoodPractices from './GoodPractices'
import LessonsLearned from './LessonsLearned'
import Navbar from './Navbar'
import Negatives from './Negatives'
import Positives from './Positives'
import Report from './Report'
import Landing from './Landing'
import Issue from './Issue'

class Renderers {
  static report(callbacks){
    return new Report(callbacks)
  }
  static improvementActions(callbacks) {
    return new ImprovementActions(callbacks)
  }
  static goodPractices(callbacks) {
    return new GoodPractices(callbacks)
  }
  static lessonsLearned(callbacks) {
    return new LessonsLearned(callbacks)
  }
  static navbar(callbacks) {
    return new Navbar(callbacks)
  }
  static negatives(callbacks) {
    return new Negatives(callbacks)
  }
  static positives(callbacks) {
    return new Positives(callbacks)
  }
  static landing (callbacks) {
    return new Landing(callbacks)
  }
  static issue (callbacks) {
    return new Issue(callbacks)
  }
}

export default Renderers
