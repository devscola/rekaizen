import React from 'react'
import ReactDOM from 'react-dom'
import Renderer from './Renderer'
import IssueComponent from '../components/Issue'

class Issue extends Renderer {
  constructor (callbacks) {
      super(callbacks)
      this.id = 'issue'
      this.data = {
        show: false,
        timeboxOptions: [],
        timeboxMoreOptions: [],
        translations: {
          issueTitle: '',
          issueSubtitle: '',
          confirm: '',
          back: '',
          confirmIssue: '',
          confirmIssueSubtitle: ''
        }
      }
  }

  update(options) {
    this.displayData(options)
  }

  translate(payload) {
    const translations = {
      issueTitle: payload.issueTitle,
      issueSubtitle: payload.issueSubtitle,
      timebox: payload.timebox,
      selectTimebox: payload.selectTimebox,
      confirm: payload.confirm,
      back: payload.back,
      confirmIssue: payload.confirmIssue,
      confirmIssueSubtitle: payload.confirmIssueSubtitle
    }
    this.displayData({translations: translations})
  }

  showCreateIssue() {
    this.displayData({show: true})
  }

  createIssue(issue) {
    this.callbacks.createIssue(issue)
    this.displayData({show: false})
  }

  draw() {
    ReactDOM.render(
      <IssueComponent
        show={this.data.show}
        id={this.id}
        createIssue={this.createIssue.bind(this)}
        translations={this.data.translations}
        timeboxOptions={this.data.timeboxOptions}
        timeboxMoreOptions={this.data.timeboxMoreOptions}
      />,
      document.querySelector('#' + this.id)
    )
  }

}

export default Issue
