import Issues from '../../../src/api/services/issues'
import Subscriber from '../../services/subscriber'
import Issue from '../../../src/api/domain/Issue'
import Bus from '../../../src/infrastructure/bus'
import Actions from '../../../src/api/actions'

describe('RemoveAnalysis Action', () => {
  let bus, issue, description

  beforeEach(() => {
    bus = new Bus()

    issue = new Issue('description', 45)
    Issues.collection.store(issue)
    description = 'A negative'
  })

  it('removes a negative', () => {
    const subscriber = new Subscriber('retrieved.negatives', bus)
    Actions.removeAnalysis(bus)
    const negative = createNegative({ description, issue: issue.id })

    bus.publish('remove.negative', { issue: issue.id, id: negative.id })

    bus.publish('retrieve.negatives', { issue: issue.id })
    expect(subscriber.hasBeenCalledWith().length).toBe(0)
  })

  function createNegative({ description, issue }) {
    Actions.createNegative(bus)
    const subscriber = new Subscriber('created.negative', bus)

    bus.publish('create.negative', { description, issue })

    return subscriber.hasBeenCalledWith()
  }
})
