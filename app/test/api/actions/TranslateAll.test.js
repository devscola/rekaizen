import Bus from '../../../src/infrastructure/bus'
import Subscriber from '../../services/subscriber'
import Actions from '../../../src/api/actions'

describe('Translate all Action', () => {
  it('retrieves all the translations', () => {
    const bus = new Bus()
    const subscriber = new Subscriber('got.translations', bus)

    Actions.translateAll(bus)

    expect(subscriber.hasBeenCalledWith().rekaizen).toBe('ReKaizen')
  })
})
