import Issues from '../../../src/api/services/issues'
import Subscriber from '../../services/subscriber'
import Issue from '../../../src/api/domain/Issue'
import Bus from '../../../src/infrastructure/bus'
import Actions from '../../../src/api/actions'

describe('RetrieveAnalyses Action', () => {
  let bus, issue, description

  beforeEach(() => {
    bus = new Bus()

    issue = new Issue('description', 45)
    Issues.collection.store(issue)
    description = 'A negative'
  })
  it('retrieves negatives', () => {
    const subscriber = new Subscriber('retrieved.negatives', bus)
    Actions.retrieveAnalyses(bus)
    createNegative({ description, issue: issue.id })

    bus.publish('retrieve.negatives', { issue: issue.id })

    expect(subscriber.hasBeenCalledWith().length).toBe(1)
    expect(subscriber.hasBeenCalledWith()[0].description).toBe(description)
  })

  function createNegative({ description, issue }) {
    Actions.createNegative(bus)
    const subscriber = new Subscriber('created.negative', bus)

    bus.publish('create.negative', { description, issue })

    return subscriber.hasBeenCalledWith()
  }
})
