import Collection from '../../src/api/services/issues/collection'
import Issue from '../../src/api/domain/Issue'
import Bus from '../../src/infrastructure/bus'
import Issues from '../../src/services/issues'
import Subscriber from './subscriber'

describe('Issues Service', () => {
  let bus, issue, description

  beforeEach(() => {
    bus = new Bus()

    description = 'An Issue'
    issue = new Issue(description, 45)
    Collection.store(issue)
  })

  it('creates a good practice', () => {
    const subscriber = new Subscriber('created.issue', bus)
    new Issues(bus)

    bus.publish('create.issue', { description: description, issue: issue.id })

    expect(subscriber.hasBeenCalledWith().description).toBe(description)
  })
})
