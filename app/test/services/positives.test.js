import Positives from '../../src/services/positives'
import Issues from '../../src/api/services/issues'
import Issue from '../../src/api/domain/Issue'
import Bus from '../../src/infrastructure/bus'
import Subscriber from './subscriber'

describe('Positives Service', () => {
  let bus, issue, description

  beforeEach(() => {
    bus = new Bus()

    issue = new Issue('description', 45)
    Issues.collection.store(issue)
    description = 'A positive'
  })

  it('creates a positive', () => {
    const subscriber = new Subscriber('created.positive', bus)
    new Positives(bus)

    bus.publish('create.positive', { description: description, issue: issue.id })

    expect(subscriber.hasBeenCalledWith().description).toBe(description)
  })

  it('retrieves the positives', () => {
    const subscriber = new Subscriber('retrieved.positives', bus)
    new Positives(bus)
    createPositive({ description, issue: issue.id })

    bus.publish('retrieve.positives', { issue: issue.id })

    expect(subscriber.hasBeenCalledWith().length).toBe(1)
    expect(subscriber.hasBeenCalledWith()[0].description).toBe(description)
  })

  it('removes the negatives', () => {
    const subscriber = new Subscriber('retrieved.positives', bus)
    new Positives(bus)
    const positive = createPositive({ description, issue: issue.id })

    bus.publish('remove.positive', { issue: issue.id, id: positive.id })

    bus.publish('retrieve.positives', { issue: issue.id })
    expect(subscriber.hasBeenCalledWith().length).toBe(0)
  })

  function createPositive({ description, issue }) {
    const subscriber = new Subscriber('created.positive', bus)

    bus.publish('create.positive', { description, issue })

    return subscriber.hasBeenCalledWith()
  }
})
