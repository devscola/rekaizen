class Callback {
  constructor() {
    this.called = false
    this.message = undefined
  }

  toBeCalled(message){
    this.called = true
    this.message = message
  }

  hasBeenCalledWith() {
    return this.message
  }

  hasBeenCalled() {
    return this.called
  }
}

export default Callback
