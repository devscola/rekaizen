const path = require('path');
const webpack = require('webpack');

module.exports = {
  mode: 'development',
  entry: './src/main.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.css$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" }
        ]
      }
    ]
   },
   resolve: {
     extensions: ['*', '.js', '.jsx']
   },
  output: {
    path: path.resolve(__dirname, 'public/app/dist'),
    filename: 'bundle.js'
  }
};
